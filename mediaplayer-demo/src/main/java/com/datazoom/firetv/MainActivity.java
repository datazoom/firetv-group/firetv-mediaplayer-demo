package com.datazoom.firetv;

import android.content.DialogInterface;
import android.graphics.PixelFormat;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.VideoView;

import com.datazoom.collector.mediaplayercollector.NativeAndroidCollector;
import com.datazoom.firetv.R;
import com.datazoom.firetv.collector.basecollector.connection_manager.DZBeaconConnector;
import com.datazoom.firetv.collector.basecollector.event_collector.collector.DZEventCollector;
import com.datazoom.firetv.collector.basecollector.model.DatazoomConfig;
import com.datazoom.firetv.collector.basecollector.model.dz_events.Event;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.File;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getCanonicalName();
    private int videoCurrentPosition;

    @BindView(R.id.txtUrl)
    EditText txtUrl;

//    @BindView(R.id.txtConfiguration)
//    EditText txtConfiguration;

    @BindView(R.id.videoView)
    VideoView videoView;

    @BindView(R.id.btnSubmit)
    Button btnSubmit;

    @BindView(R.id.btnPush)
    Button btnPush;

    @BindView(R.id.txtVersion)
    TextView txtVersion;

    @BindView(R.id.pbBuffering)
    ProgressBar pbBuffering;

    private MediaController mediacontroller;
    private int mCurrentPosition = 0;
    private static final String PLAYBACK_TIME = "play_time";
    String url = "https://www.radiantmediaplayer.com/media/bbb-360p.mp4";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
        btnPush.setVisibility(View.INVISIBLE);
        pbBuffering.setVisibility(View.GONE);
        txtVersion.setText("Demo - " + BuildConfig.VERSION_NAME + " | Library - " + com.datazoom.collector.mediaplayercollector.BuildConfig.VERSION_NAME + " | Framework - " + com.datazoom.collector.mediaplayercollector.BuildConfig.VERSION_NAME);

        if (savedInstanceState != null) {
            mCurrentPosition = savedInstanceState.getInt(PLAYBACK_TIME);
        }

        btnSubmit.setOnClickListener(v -> {
            btnPush.setVisibility(View.VISIBLE);
            btnSubmit.setVisibility(View.INVISIBLE);
            btnSubmit.setEnabled(false);
            pbBuffering.setVisibility(View.VISIBLE);

            Uri video = Uri.parse(url);

            getWindow().setFormat(PixelFormat.TRANSLUCENT);
            MediaController mediaController = new MediaController(MainActivity.this);
            mediaController.setAnchorView(videoView);

            videoView.setMediaController(mediaController);
            videoView.setVideoURI(video);
            videoView.requestFocus();

            videoView.setOnPreparedListener(mp -> {

                String[] url = txtUrl.getText().toString().split(" configId:");
                String configId = url[1];
                String configUrl = url[0];

                NativeAndroidCollector
                        .create(this, mp, videoView)
                        .setConfig(new DatazoomConfig(configId, configUrl))
                        .connect(new DZBeaconConnector.ConnectionListener(){
                                     @Override
                                     public void onSuccess(DZEventCollector dzEventCollector) {
                                         try {
                                             dzEventCollector.setDatazoomMetadata(new JSONArray( "[{\"customPlayerName\": \"Native Media Player\"},{\"customDomain\": \"devplatform.io\"}]  "));
                                         } catch (JSONException e) {
                                             e.printStackTrace();
                                         }

                                         Event event = new Event("SDKLoaded",new JSONArray());
                                         dzEventCollector.addCustomEvent(event);

                                         btnPush.setOnClickListener(v -> {

                                             try {
                                                 Event event1 = new Event("buttonPush",new JSONArray( "[{\"customPlay\": \"true\"},{\"customPause\": \"false\"}] ") );
                                                 dzEventCollector.addCustomEvent(event1);
                                             } catch (JSONException e) {
                                                 e.printStackTrace();
                                             }


                                         });

                                         pbBuffering.setVisibility(View.GONE);
                                         mediacontroller = new MediaController(MainActivity.this);
                                         mediacontroller.setMediaPlayer(videoView);
                                         mediacontroller.setAnchorView(videoView);
                                         videoView.setMediaController(mediacontroller);
                                         mediacontroller.show(0);
                                     }

                                     @Override
                                     public void onError(Throwable t) {
                                         pbBuffering.setVisibility(View.GONE);
                                         btnSubmit.setEnabled(true);
                                         showAlert("Error", "Error while creating NativeAndroidConnector, error:" + t.getMessage());

                                     }
                                 }
                        );
            });
        });
        try {
            LogMonitor.startAppendingLogsTo(new File(getFilesDir(), "datazoom-log.log"));
        } catch (IOException e) {
            Log.e(TAG, "Cannot create file:datazoom-log.log");
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt(PLAYBACK_TIME, videoView.getCurrentPosition());
    }

    private void initializePlayer() {
        if (mCurrentPosition > 0) {
            videoView.seekTo(mCurrentPosition);
        }
    }

    private void releasePlayer() {
        if (mediacontroller != null) {
            videoView.stopPlayback();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        initializePlayer();
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onStop() {
        if (mediacontroller != null)
            releasePlayer();
        super.onStop();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        LogMonitor.stopMonitoring();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (videoView != null)
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
                videoView.pause();
            }
    }

    private void showAlert(String title, String message) {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
}

